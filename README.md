# TP-UNITTESTING-CALCULATOR

This is a calculator with the objective of respecting the necessary parameters to validate a college project. It has several features that will be detailed later on.

## Development Environnement

## Pulling Repository

Chose any path in your system and clone the git repository

```
HTTP: 
git clone https://gitlab.com/epsi-personal-projects/b3-bachelor/5.-testing/b3-c3-dev-tu-pinto-jose.git
```
```
SSH:
ssh-keygen -t rsa (Add the sshkey to your gitlab account)

git clone git@gitlab.com:epsi-personal-projects/b3-bachelor/5.-testing/b3-c3-dev-tu-pinto-jose.git
```


## Running Docker with the bot
The first thing to do will be to create our custom docker image. (be sure you are inside the repository you just cloned)

```
docker build -t b3-c3-dev-tu-pinto-jose:1.0 .
```

The command bellow will bound the repository that you just cloned with the volume inside the container.

```
docker run -dti -v $(pwd):/home/b3-c3-dev-tu-pinto-jose --name b3-c3-dev-tu-pinto-jose -p 4200:4200 b3-c3-dev-tu-pinto-jose:1.0
```

Installation of the npm modules inside of the container

```
docker exec -ti b3-c3-dev-tu-pinto-jose npm install
```

Building the project

```
docker exec -ti b3-c3-dev-tu-pinto-jose ng build
```

Running the project

```
docker exec -ti b3-c3-dev-tu-pinto-jose npm run dev
```

Unit testing

```
docker exec -ti b3-c3-dev-tu-pinto-jose npm test
```

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
