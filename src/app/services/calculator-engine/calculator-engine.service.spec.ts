import { TestBed } from '@angular/core/testing';

import { CalculatorEngineService } from './calculator-engine.service';

describe('CalculatorEngineService', () => {
  let service: CalculatorEngineService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CalculatorEngineService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('Add value (Positive Values)', () => {
    expect(service.add(15, 15)).toEqual(30);
  })

  it('Add value (Negative Values)', () => {
    expect(service.add(-15, -15)).toEqual(-30);
  })

  it('Substract value (Positive Values)', () => {
    expect(service.subtract(50, 15)).toEqual(35);
  })

  it('Substract value (Negative Values)', () => {
    expect(service.subtract(-50, -15)).toEqual(-35);
  })

  it('Multiply value (Positive Values)', () => {
    expect(service.multiply(5, 5)).toEqual(25);
  })

  it('Multiply value (Negative Values)', () => {
    expect(service.multiply(-50, 5)).toEqual(-250);
  })

  it('Divide value (Positive Values)', () => {
    expect(service.divide(100, 2)).toEqual(50);
  })

  it('Divide value (Negative Values)', () => {
    expect(service.divide(-50, 2)).toEqual(-25);
  })

  it('Power value (Positive Values)', () => {
    expect(service.power(6, 5)).toEqual(7776);
  })

  it('Power value (Negative Values)', () => {
    expect(service.power(-2, 5)).toEqual(-32);
  })

  it('SquareRoot value (Positive Values)', () => {
    expect(service.squareRoot(9)).toEqual(3);
  })
});
