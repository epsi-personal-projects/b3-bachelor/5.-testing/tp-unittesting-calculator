import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CalculatorEngineService {

  constructor() { }

  public add(x: number, y: number): number {
    return x + y;
  }

  public subtract(x: number, y: number): number {
    return x - y;
  }

  public divide(x: number, y: number): number {
    return x / y;
  }

  public multiply(x: number, y: number): number {
    return x * y;
  }

  public power(x: number, y: number): number {
    return Math.pow(x, y);
  }

  public squareRoot(x: number): number {
    return Math.sqrt(x);
  }
}
