import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CalculatorComponent } from './calculator.component';

describe('CalculatorComponent', () => {
  let component: CalculatorComponent;
  let fixture: ComponentFixture<CalculatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CalculatorComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CalculatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Test changed Operation', () => {
    component.setOperation("*");

    expect(component.getOperation()).toEqual("*");

    component.setOperation("/")

    expect(component.getOperation()).toEqual("/");
  })

  it('Save selected number to accumulator', () => {
    component.setNumber(65);
    component.saveSelectedToAccumulator();

    expect(component.getAccumulator()).toEqual(65);
  })

  it('Add Number', () => {
    // Set first digits to the current selected numbers
    component.setNumber(95);

    // Set Add Operation && set the current selected numbers to the accumulator
    component.add();

    expect(component.getAccumulator()).toEqual(95);

    expect(component.getOperation()).toEqual("+");

    // Set second digits to the current selected numbers
    component.setNumber(10);

    expect(component.getSelectedNumber()).toEqual(10);

    //Conclude the calculation
    component.finishCalculation();

    //The result is set to the SelectedNumber
    expect(component.getSelectedNumber()).toEqual(105);
  })

  it('Substract Number', () => {
    // Set first digits to the current selected numbers
    component.setNumber(100);

    // Set Add Operation && set the current selected numbers to the accumulator
    component.subtract();

    expect(component.getAccumulator()).toEqual(100);

    expect(component.getOperation()).toEqual("-");

    // Set second digits to the current selected numbers
    component.setNumber(50);

    expect(component.getSelectedNumber()).toEqual(50);

    //Conclude the calculation
    component.finishCalculation();

    //The result is set to the SelectedNumber
    expect(component.getSelectedNumber()).toEqual(50);
  })
});
