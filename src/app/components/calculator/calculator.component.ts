import { Component, OnInit } from '@angular/core';
import { CalculatorEngineService } from 'src/app/services/calculator-engine/calculator-engine.service';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss']
})
export class CalculatorComponent implements OnInit {

  public selectedNumber: string = "0";
  public accumulator: number = 0;
  public executedOperation: string = "";

  constructor(
    private engine: CalculatorEngineService
  ) { }

  ngOnInit(): void {
  }

  /* -------------------------------------------------------------------------- */

  public getSelectedNumber(): number {
    return Number(this.selectedNumber);
  }

  public getAccumulator(): number {
    return this.accumulator;
  }

  public setNumber(number: number): void {
    if(this.selectedNumber === "0") this.selectedNumber = "";
    this.selectedNumber += number;
  }

  public removeLastNumber(): void {
    this.selectedNumber = this.selectedNumber.slice(0, -1);
  }

  public addComma(): void {
    this.selectedNumber = this.selectedNumber + ".";
  }

  public saveSelectedToAccumulator(): void {
    if (this.selectedNumber.includes("√")) 
      this.selectedNumber = String(this.engine.squareRoot(Number(this.selectedNumber.replace("√", ""))));
      
    this.accumulator = this.getSelectedNumber();
  }

  public setOperation(operation: string): void {
    this.executedOperation = operation;
  }

  public getOperation(): string {
    return this.executedOperation;
  }

  public resetCalculator(): void {
    this.selectedNumber = "0";
    this.accumulator = 0;
  }

  public resetAccumulator(): void {
    this.accumulator = 0;
  }

  public resetOperation(): void {
    this.executedOperation = "";
  }

  /* -------------------------------------------------------------------------- */

  public add(): void {
    this.saveSelectedToAccumulator();
    this.selectedNumber = "0";
    this.setOperation("+");
  }

  public subtract(): void {
    if(this.selectedNumber === "0"){
      this.selectedNumber = "-";
    } else {
      this.saveSelectedToAccumulator();
      this.selectedNumber = "0";
      this.setOperation("-");
    }
  }

  public divide(): void {
    this.saveSelectedToAccumulator();
    this.selectedNumber = "0";
    this.setOperation("/");
  }

  public multiply(): void {
    this.saveSelectedToAccumulator();
    this.selectedNumber = "0";
    this.setOperation("*");
  }

  public power(): void {
    this.saveSelectedToAccumulator();
    this.selectedNumber = "0";
    this.setOperation("^");
  }

  public squareRoot(): void {
    this.selectedNumber = "√";
  }

  /* -------------------------------------------------------------------------- */

  public finishCalculation(): void {
    if (this.selectedNumber.includes("√")) 
      this.selectedNumber = String(this.engine.squareRoot(Number(this.selectedNumber.replace("√", ""))));
    
    switch (this.getOperation()) {
        case "+":
            let addResult = this.engine.add(this.getAccumulator(), this.getSelectedNumber());
            this.selectedNumber = String(addResult);
            break;
        case "-":
            let subtractResult = this.engine.subtract(this.getAccumulator(), this.getSelectedNumber());
            this.selectedNumber = String(subtractResult);
            break;
        case "/":
            let divideResult = this.engine.divide(this.getAccumulator(), this.getSelectedNumber());
            this.selectedNumber = String(divideResult);
            break;
        case "*":
            let multiplyResult = this.engine.multiply(this.getAccumulator(), this.getSelectedNumber());
            this.selectedNumber = String(multiplyResult);
            break;
        case "^":
            let powerResult = this.engine.power(this.getAccumulator(), this.getSelectedNumber());
            this.selectedNumber = String(powerResult);
            break;
        default:
            break;
    }
    this.resetAccumulator();
    this.resetOperation();
  }
}
